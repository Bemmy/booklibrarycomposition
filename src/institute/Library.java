package institute;

import java.util.*;

public class Library {
    
    private String name;
    private final  List<Book> books; 
    
    public Library(String name, List<Book> books){
        setName(name);
        this.books = books; 
    }
    
    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public List<Book> getTotalBooksInLibrary(){
        return books;
    }
    
}
