package institute;

import java.util.*;

public class BookLibrarySimulation {

    public static void main(String[] args) {
        
        Book book1 = new Book("The Great Gatsby", "F. Scott Fitzgerald");
        Book book2 = new Book("Sapiens", "Yuval Noah Harari");
        Book book3 = new Book("Five Feet Apart", "Rachael Lippincott");
                
        List<Book> books = new ArrayList<Book>();
        
        books.add(book1);
        books.add(book2);
        books.add(book3);
        
        Library library = new Library("Boston Central Library", books);
        
        List<Book> bks = library.getTotalBooksInLibrary();
        
        for(Book bk : bks){
             System.out.println("Title: " + bk.getTitle() + " Author: " + 
                bk.getAuthor());
        }
          
    }
}
